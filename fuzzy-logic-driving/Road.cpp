#include "Road.h"

#include <string>

#include "Graphics.h"
#include "Config.h"

namespace {

const std::string c_filename("Road");
const int c_sourceWidth = 1080;
const int c_sourceHeight = 1080;

const units::Velocity c_velocity = 0.45f;

}

Road::Road( Graphics& graphics )
    : m_sprite(graphics, c_filename, 0, 0, c_sourceWidth, c_sourceHeight)
    , m_kinematics(0, c_velocity)
    , m_kinematics2(-c_sourceHeight, c_velocity)
{
}

void Road::update( units::MS deltaTime )
{
    m_kinematics.position += m_kinematics.delta(deltaTime);
    if (m_kinematics.position > c_sourceHeight) {
	m_kinematics.position = -c_sourceHeight;
    }
}

void Road::draw( Graphics& graphics )
{
    m_sprite.draw(graphics, 0.0f, m_kinematics.position - c_sourceHeight);
    m_sprite.draw(graphics, 0.0f, m_kinematics.position);
    m_sprite.draw(graphics, 0.0f, m_kinematics.position + c_sourceHeight);
}
