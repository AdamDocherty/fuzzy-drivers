#ifndef Sprite_h__
#define Sprite_h__

#include <string>
#include <SDL.h>

#include "Units.h"

class Graphics;

class Sprite {
public:
    Sprite(Graphics& graphics, const std::string& filename, units::Pixel sourceX, units::Pixel sourceY,
		units::Pixel sourceWidth, units::Pixel sourceHeight);

    virtual void update() {}
    void draw(Graphics& graphics, units::Game x, units::Game y, units::Degrees angle = 0.0f, SDL_RendererFlip = SDL_FLIP_NONE);
    void setSize(units::Pixel x, units::Pixel y);
    void setColourMod(Uint8 red, Uint8 green, Uint8 blue);
protected:
    SDL_Rect m_sourceRect;
    SDL_Point m_size;
private:
    SDL_Texture* m_spriteSheet;
};

#endif // Sprite_h__
