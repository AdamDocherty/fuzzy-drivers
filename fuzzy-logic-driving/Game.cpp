#include "Game.h"

#include <SDL.h>
#include <stdlib.h>
#include <time.h>

#include "Config.h"

#include "FuzzySteeringController.h"

#include "Graphics.h"
#include "Sprite.h"
#include "Car.h"
#include "RacingLine.h"
#include "Road.h"
#include "Input.h"
#include "Timer.h"

namespace {
    const units::FPS c_fps = 60;
    const units::MS c_maxFrameTime = 5 * 1000 / c_fps;
}

Game::Game(const FuzzySteeringController& fuzzyController) 
    : m_fuzzyController(fuzzyController)
{
    srand(static_cast<unsigned int>(time(NULL)));
    SDL_Init(SDL_INIT_EVERYTHING);
    eventLoop();
}

Game::~Game() {
    SDL_Quit();
}

void Game::eventLoop() {
    Graphics graphics;
    Input input;
    SDL_Event event;

    m_car.reset(new Car(graphics, config::c_screenWidth * 0.5f, config::c_screenHeight * 0.5f, m_fuzzyController));
    m_racingLine.reset(new RacingLine(graphics, config::c_screenWidth * 0.5f));
    m_road.reset(new Road(graphics));
    m_instructionsSprite.reset(new Sprite(graphics, "Instructions", 0, 0, 400, 200));

    bool running = true;
    units::MS lastUpdateTime = SDL_GetTicks();
    while (running) {
        const units::MS startTimeMS = SDL_GetTicks();
        input.beginNewFrame();
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_KEYDOWN:
                input.keyDownEvent(event);
                break;
            case SDL_KEYUP:
                input.keyUpEvent(event);
                break;
            case SDL_QUIT:
                running = false;
                break;
            default:
                break;
            }
        }

        if (input.wasKeyPressed(SDLK_ESCAPE)) {
            running = false;
        }

	handleInput(input);

        const units::MS currentTimeMS = SDL_GetTicks();
        const units::MS elapsedTime = currentTimeMS - lastUpdateTime;
        update(std::min(elapsedTime, c_maxFrameTime), graphics);
        lastUpdateTime = currentTimeMS;

        draw(graphics);
        const units::MS msPerFrame = 1000/*ms*/ / c_fps;
        const units::MS elapsedTimeMS = SDL_GetTicks() - startTimeMS;
        if (elapsedTimeMS < msPerFrame) {
            SDL_Delay(msPerFrame - elapsedTimeMS);
        }
    }
}

void Game::handleInput(Input& input) {
    // Car movement
    if (input.isKeyHeld(SDLK_a) && input.isKeyHeld(SDLK_d)) {
        m_car->stopMoving();
    } else if (input.isKeyHeld(SDLK_a)) {
        m_car->startMovingLeft();
    } else if (input.isKeyHeld(SDLK_d)) {
        m_car->startMovingRight();
    } else {
        m_car->stopMoving();
    }

    // Racing Line movement
    if (input.isKeyHeld(SDLK_LEFT) && input.isKeyHeld(SDLK_RIGHT)) {
        m_racingLine->stopMoving();
    } else if (input.isKeyHeld(SDLK_LEFT)) {
        m_racingLine->startMovingLeft();
    } else if (input.isKeyHeld(SDLK_RIGHT)) {
        m_racingLine->startMovingRight();
    } else {
        m_racingLine->stopMoving();
    }

    // Racing line speed
    if (input.isKeyHeld(SDLK_9)) {
	m_racingLine->setSpeed(9);
    } else if (input.isKeyHeld(SDLK_8)) {
	m_racingLine->setSpeed(8);
    } else if (input.isKeyHeld(SDLK_7)) {
	m_racingLine->setSpeed(7);
    } else if (input.isKeyHeld(SDLK_6)) {
	m_racingLine->setSpeed(6);
    } else if (input.isKeyHeld(SDLK_5)) {
	m_racingLine->setSpeed(5);
    } else if (input.isKeyHeld(SDLK_4)) {
	m_racingLine->setSpeed(4);
    } else if (input.isKeyHeld(SDLK_3)) {
	m_racingLine->setSpeed(3);
    } else if (input.isKeyHeld(SDLK_2)) {
	m_racingLine->setSpeed(2);
    } else {
	m_racingLine->setSpeed(1);
    }

    // Chose steering type
    if (input.wasKeyPressed(SDLK_q)) {
        m_car->setManualSteering();
	m_racingLine->setColour(0xff, 0xff, 0);
    } else if (input.wasKeyPressed(SDLK_w)) {
	m_car->setBasicAutoSteering();
	m_racingLine->setColour(0xff, 0, 0);
    } else if (input.wasKeyPressed(SDLK_e)) {
	m_car->setFuzzyAutoSteering();
	m_racingLine->setColour(0, 0xff, 0);
    }

    if (input.wasKeyPressed(SDLK_c)) {
	m_car->toggleFuzzySpeedLimit();
    }
#ifdef _DEBUG
    if (input.wasKeyPressed(SDLK_p)) {
        m_car->toggleDebugInfo();
    }
#endif//_DEBUG
}

void Game::update(units::MS deltaTimeMS, Graphics& graphics) {
    Timer::updateAll(deltaTimeMS);
    m_racingLine->update(deltaTimeMS);
    m_car->update(deltaTimeMS, *m_racingLine);
    m_road->update(deltaTimeMS);
}

void Game::draw(Graphics& graphics) {
    graphics.clear();
    m_road->draw(graphics);
    m_racingLine->draw(graphics);
    m_car->draw(graphics);
    m_instructionsSprite->draw(graphics, 10, config::c_screenHeight - 200 - 10);
    graphics.flip();
}