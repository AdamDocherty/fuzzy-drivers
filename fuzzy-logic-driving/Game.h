#ifndef Game_h__
#define Game_h__

#include <memory>
#include <vector>

#include "Units.h"

class FuzzySteeringController;

class Car;
class RacingLine;
class Road;
class Graphics;
class Sprite;
class Input;

class Game {
public:
    Game(const FuzzySteeringController& fuzzyController);
    ~Game();

private:
    void eventLoop();
    void handleInput(Input& input);
    void update(units::MS deltaTimeMS, Graphics& graphics);
    void draw(Graphics& graphics);

    std::shared_ptr<Car> m_car;
    std::shared_ptr<RacingLine> m_racingLine;
    std::shared_ptr<Road> m_road;

    std::shared_ptr<Sprite> m_instructionsSprite;

    const FuzzySteeringController& m_fuzzyController;
};

#endif // Game_h__
