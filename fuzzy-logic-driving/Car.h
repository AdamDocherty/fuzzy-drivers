#ifndef Car_h__
#define Car_h__

#include <memory>

#include "Kinematics.h"
#include "Sprite.h"
#include "Timer.h"
#include "Units.h"

class FuzzySteeringController;

class Accelerator;
class Graphics;
class RacingLine;

class Car {
public:
    Car(Graphics& graphics, units::Game x, units::Game y, const FuzzySteeringController& fuzzyController);

    void update(units::MS deltaTimeMS, const RacingLine& racingLine);
    void draw(Graphics& graphics);

    void startMovingLeft();
    void startMovingRight();
    void stopMoving();

    void setManualSteering();
    void setBasicAutoSteering();
    void setFuzzyAutoSteering();

    units::Game centerX() const;
    units::Game centerY() const;

    inline void toggleFuzzySpeedLimit() { m_fuzzySpeedLimit = !m_fuzzySpeedLimit; } 
#ifdef _DEBUG
    inline void toggleDebugInfo() { m_showDebugInfo = !m_showDebugInfo; }
#endif//_DEBUG
private:
    enum DirectionType {
	DIRECTION_STRAIGHT = 0,
	DIRECTION_RIGHT, 
	DIRECTION_LEFT
    };
    // TODO: Add enums for different types of fuzzy logic controls
    enum ControlType {
	CONTROL_MANUAL = 0, 
	CONTROL_BASIC_AUTO,
	CONTROL_FUZZY_AUTO
    };

    void updateManual( units::MS deltaTimeMS );
    void updateBasicAuto( units::MS deltaTimeMS, const RacingLine& racingLine );
    void updateFuzzyAuto( units::MS deltaTimeMS, const RacingLine& racingLine );

    void updatePosition( units::MS deltaTimeMS, const Accelerator& accelerator );

    Kinematics m_kinematicsX, m_kinematicsY;
    DirectionType m_direction;
    units::Degrees m_angle;

    Sprite m_sprite;

    ControlType m_controlType;

    const FuzzySteeringController& m_fuzzyController;

    bool m_fuzzySpeedLimit;
#ifdef _DEBUG
    Timer m_debugPrintTimer;
    bool m_showDebugInfo;
#endif//_DEBUG
};

#endif // Car_h__
