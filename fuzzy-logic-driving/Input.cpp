#include "Input.h"

void Input::beginNewFrame()
{
    m_pressedKeys.clear();
    m_releasedKeys.clear();
}

void Input::keyDownEvent( const SDL_Event& event )
{
    m_pressedKeys[event.key.keysym.sym] = true;
    m_heldKeys[event.key.keysym.sym] = true;
}

void Input::keyUpEvent( const SDL_Event& event )
{
    m_releasedKeys[event.key.keysym.sym] = true;
    m_heldKeys[event.key.keysym.sym] = false;
}

bool Input::wasKeyPressed( SDL_Keycode key )
{
    return m_pressedKeys[key];
}

bool Input::wasKeyReleased( SDL_Keycode key )
{
    return m_releasedKeys[key];
}

bool Input::isKeyHeld( SDL_Keycode key )
{
    return m_heldKeys[key];
}
