#ifndef Accelerators_h__
#define Accelerators_h__

#include "Units.h"

class Kinematics;

class Accelerator {
public:
    virtual void updateVelocity(Kinematics& kinematics, units::MS deltaTime) const = 0;
    virtual ~Accelerator() = 0;
};
inline Accelerator::~Accelerator() {}

class ZeroAccelerator : public Accelerator {
public:
    ZeroAccelerator() {}
    void updateVelocity(Kinematics& kinematics, units::MS deltaTime) const {}
    static const ZeroAccelerator c_zero;
};

class FrictionAccelerator : public Accelerator {
public:
    FrictionAccelerator(units::Acceleration friction) :
        m_friction(friction) {}
        void updateVelocity( Kinematics& kinematics, units::MS deltaTime ) const;
private:
    const units::Acceleration m_friction;
};

class ConstantAccelerator : public Accelerator {
public:
    ConstantAccelerator(units::Acceleration acceleration, units::Velocity maxVelocity) :
        m_acceleration(acceleration), m_maxVelocity(maxVelocity) {}
    void updateVelocity(Kinematics& kinematics, units::MS deltaTime) const;
private:
    const units::Acceleration m_acceleration;
    const units::Velocity m_maxVelocity;
};

class BidirectionalAccelerator {
public:
    BidirectionalAccelerator(units::Acceleration acceleration, units::Velocity maxVelocity) :
        positive(acceleration, maxVelocity),
	negative(-acceleration, -maxVelocity) {}

    ConstantAccelerator positive, negative;
};

#endif // Accelerators_h__
