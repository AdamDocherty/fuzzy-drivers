#ifndef FuzzySteeringController_h__
#define FuzzySteeringController_h__

#include <fl/Headers.h>
#include <memory>

class FuzzySteeringController {
public:
    FuzzySteeringController();
    ~FuzzySteeringController();

    //! Calculate the acceleration using the fuzzy logic system
    //! @param distance The distance between the car and the racing line
    //!		Should be in the range [-c_screenWidth, c_screenWidth], if outside this range will be clamped
    //!		Will be normalised to a number in the range [-1, 1] to be used in the fuzzy logic calculation
    //! @param rateOfChange The rate of change of the distance between the car and the racing line
    //!		Will be clamped to the range (-1.8, 1.8) so it will not go out of range of the fuzzy sets
    fl::scalar calculateAcceleration(fl::scalar distance, fl::scalar rateOfChange) const;

    void printAllResults() const;

private:
    std::shared_ptr<fl::Engine> m_engine;
    fl::InputVariable* m_distanceInput;
    fl::InputVariable* m_rateOfChangeInput;
    fl::OutputVariable* m_accelerationOutput;

};

#endif // FuzzySteeringController_h__
