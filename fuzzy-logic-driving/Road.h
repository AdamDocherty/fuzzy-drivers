#ifndef Road_h__
#define Road_h__

#include "Units.h"
#include "Sprite.h"
#include "Kinematics.h"

class Graphics;

class Road {
public:
    Road(Graphics& graphics);

    void update(units::MS deltaTime);
    void draw(Graphics& graphics);

private:
    Sprite m_sprite;
    Kinematics m_kinematics, m_kinematics2;
};

#endif // Road_h__
