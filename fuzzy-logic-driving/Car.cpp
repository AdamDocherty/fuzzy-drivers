#include "Car.h"

#include <string>

#include "Config.h"

#include "FuzzySteeringController.h"

#include "Accelerators.h"
#include "RacingLine.h"

namespace {

const std::string c_spriteName("Car");
const int c_sourceX = 0;
const int c_sourceY = 0;
const int c_sourceWidth = 162;
const int c_sourceHeight = 256;

// Basic steering motion
const units::Acceleration c_acceleration = 0.0012f; // acceleration  for manual and basic auto modes
const units::Velocity c_maxSpeedX = 0.30f; // max speed for manual and basic auto modes
const BidirectionalAccelerator c_basicAccelerator(c_acceleration, c_maxSpeedX);

// Stopping motion
const units::Acceleration c_friction = 0.0004f;
const FrictionAccelerator c_frictionAccelerator(c_friction);

// The distance at which the car will stop accelerating towards the racing line
// in the basic auto mode
const units::Game c_basicAutoCutoff = 10.0f;

// No speed limit when being controlled by the fuzzy system
const units::Velocity c_maxSpeedFuzzyX = 0.30f;

const float c_rotation = 100.0f;

// Normalise the distance so that a value of 1.8 is equal to the length of the screen
units::Game normaliseDistance(units::Game distance) {
    static const units::Game distanceScaleFactor = 1.8f / config::c_screenWidth;
    return distance * distanceScaleFactor;
}

}

Car::Car( Graphics& graphics, units::Game x, units::Game y, const FuzzySteeringController& fuzzyController )
    : m_kinematicsX(x, 0.0f)
    , m_kinematicsY(y, 0.0f)
    , m_direction(DIRECTION_STRAIGHT)
    , m_angle(0.0f)
    , m_sprite(graphics, c_spriteName, c_sourceX, c_sourceY, c_sourceWidth, c_sourceHeight)
    , m_controlType(CONTROL_FUZZY_AUTO)
    , m_fuzzyController(fuzzyController)
    , m_fuzzySpeedLimit(true)
#ifdef _DEBUG
    , m_debugPrintTimer(1000, true)
    , m_showDebugInfo(true)
#endif//_DEBUG
{

}

void Car::update( units::MS deltaTimeMS, const RacingLine& racingLine )
{
    switch(m_controlType) {
    case CONTROL_MANUAL:
	updateManual(deltaTimeMS);
	break;
    case CONTROL_BASIC_AUTO:
	updateBasicAuto(deltaTimeMS, racingLine);
	break;
    case CONTROL_FUZZY_AUTO:
	updateFuzzyAuto(deltaTimeMS, racingLine);
	break;
    default:
	break;
    }

    // Keep the car on the screen
    if (m_kinematicsX.position > config::c_screenWidth) {
	m_kinematicsX.position = config::c_screenWidth;
    } else if (m_kinematicsX.position < 0.0f) {
	m_kinematicsX.position = 0.0f;
    }

    m_angle = m_kinematicsX.velocity * c_rotation;
    m_angle = std::max(-80.0f, std::min(m_angle, 80.0f));

#ifdef _DEBUG
    if (m_showDebugInfo && m_debugPrintTimer.expired()) {
	printf("Car Details: x=%.2f, vx=%f, y=%.2f\n", m_kinematicsX.position, m_kinematicsX.velocity, m_kinematicsY.position);
	m_debugPrintTimer.reset();
    }
#endif//_DEBUG
}

void Car::draw( Graphics& graphics )
{
    m_sprite.draw(graphics, centerX(), centerY(), m_angle);
}

void Car::startMovingLeft()
{
    m_direction = DIRECTION_LEFT;
}

void Car::startMovingRight()
{
    m_direction = DIRECTION_RIGHT;    
}

void Car::stopMoving()
{
    m_direction = DIRECTION_STRAIGHT;
}

void Car::setManualSteering()
{
    if (m_controlType != CONTROL_MANUAL) {
	m_controlType = CONTROL_MANUAL;
        printf("Manual Steering Enabled.\n");
    }
}

void Car::setBasicAutoSteering()
{
    if (m_controlType != CONTROL_BASIC_AUTO) {
	m_controlType = CONTROL_BASIC_AUTO;
        printf("Basic Auto Steering Enabled.\n");
    }
}

void Car::setFuzzyAutoSteering()
{
    if (m_controlType != CONTROL_FUZZY_AUTO) {
	m_controlType = CONTROL_FUZZY_AUTO;
        printf("Fuzzy Auto Steering Enabled.\n");
    }
}

void Car::updateManual( units::MS deltaTimeMS )
{
    const Accelerator* accelerator;
    if (m_direction == DIRECTION_STRAIGHT) {
	accelerator = &c_frictionAccelerator;
    } else if (m_direction == DIRECTION_LEFT) {
	accelerator = &c_basicAccelerator.negative;
    } else { // m_direction == DIRECTION_RIGHT
	accelerator = &c_basicAccelerator.positive;
    }
    updatePosition(deltaTimeMS, *accelerator);
}

void Car::updateBasicAuto( units::MS deltaTimeMS, const RacingLine& racingLine )
{
    const Accelerator* accelerator;
    units::Game distance = racingLine.getX() - m_kinematicsX.position;
    if (distance > c_basicAutoCutoff) {
	accelerator = &c_basicAccelerator.positive;
    } else if (distance < - c_basicAutoCutoff) {
	accelerator = &c_basicAccelerator.negative;
    } else {
	accelerator = &c_frictionAccelerator;
    }
    updatePosition(deltaTimeMS, *accelerator);
}

void Car::updateFuzzyAuto( units::MS deltaTimeMS, const RacingLine& racingLine )
{
    units::Game distance = m_kinematicsX.position - racingLine.getX();
    units::Game rateOfChange = m_kinematicsX.velocity - racingLine.getRateOfChange();

    const units::Game normalisedDistance = std::max(-1.799f, std::min(normaliseDistance(distance), 1.799f));
    const units::Game normalisedRateOfChange = std::max(-1.799f, std::min(rateOfChange, 1.799f));

    units::Acceleration acceleration = static_cast<units::Acceleration>
        (m_fuzzyController.calculateAcceleration(normalisedDistance, normalisedRateOfChange));

    units::Velocity speedLimit = (m_fuzzySpeedLimit) ? c_maxSpeedFuzzyX : FLT_MAX;

#ifdef _DEBUG
    if (m_showDebugInfo && m_debugPrintTimer.expired()) {
	std::cout << std::setprecision(4) << "Distance = " << distance << " (" << normalisedDistance << "), " <<
		"Rate Of Change = " << rateOfChange << " (" << normalisedRateOfChange << ") -> " << std::endl << 
		"\tAcceleration = " << acceleration << std::endl;
	m_debugPrintTimer.reset(); // reseting the timer here will stop the standard car info from being shown
    }
#endif //_DEBUG

    if (acceleration > 0.0f) {
	ConstantAccelerator accelerator(acceleration * 0.1f, speedLimit);
    	updatePosition(deltaTimeMS, accelerator);
    } else if (acceleration < 0.0f) {
	ConstantAccelerator accelerator(acceleration * 0.1f, -speedLimit);
    	updatePosition(deltaTimeMS, accelerator);
    } else {
	return;
    }
}

void Car::updatePosition( units::MS deltaTimeMS, const Accelerator& accelerator )
{
    accelerator.updateVelocity(m_kinematicsX, deltaTimeMS);
    const units::Game delta = m_kinematicsX.delta(deltaTimeMS);
    m_kinematicsX.position += delta;
}

units::Game Car::centerX() const
{
    return m_kinematicsX.position - (c_sourceWidth / 2);
}

units::Game Car::centerY() const
{
    return m_kinematicsY.position - (c_sourceHeight / 2);
}

