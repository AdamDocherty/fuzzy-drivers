#ifndef Timer_h__
#define Timer_h__

#include <set>

#include "Units.h"

class Timer {
public:
    Timer(units::MS expirationTime, bool startActive = false);
    ~Timer();

    void reset();
    bool active() const;
    bool expired() const;

    units::MS currentTime() const;

    static void updateAll(units::MS deltaTime);

private:
    void update(units::MS deltaTime);

    units::MS m_curentTime;
    const units::MS m_expirationTime;

    static std::set<Timer*> s_timers;

};

#endif // Timer_h__
