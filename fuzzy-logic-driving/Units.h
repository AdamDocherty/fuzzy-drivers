#ifndef Units_h__
#define Units_h__

namespace units {

typedef float Game; //! Float for extra precision. 
typedef int Pixel; //! Integer for discrete units. Pixel values can be +/-.
typedef unsigned int Frame; //! Discrete. Non-negative

typedef unsigned int MS; //! Discrete Milliseconds. unsigned int matches SDL.
typedef unsigned int FPS; //! Frames per second (Hz)

typedef float Velocity; //! Game / MS
typedef float Acceleration; //! Velocity / MS

typedef float Degrees;
typedef float Radians;

namespace {
int round(double val) {
    return (int)(val > 0.0 ? val + 0.5 : val - 0.5);
}
int round(float val) {
    return (int)(val > 0.0f ? val + 0.5f : val - 0.5f);
}
}

inline Pixel gameToPixel(Game game) {
    // Using rounding here seems to cause issues with seamless textures
    //return Pixel(round(game));
    return Pixel(game);
}

}

#endif // Units_h__
