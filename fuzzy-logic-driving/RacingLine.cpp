#include "RacingLine.h"

#include <algorithm>

#include "Config.h"
#include "Graphics.h"

namespace {

const std::string c_spriteName("RacingLine");
const int c_sourceX = 0;
const int c_sourceY = 0;
const int c_sourceWidth = 32;
const int c_sourceHeight = 32;

const units::Velocity c_speed = 0.1f;

}

RacingLine::RacingLine( Graphics& graphics, units::Game x )
    : m_sprite(graphics, c_spriteName, c_sourceX, c_sourceY, c_sourceWidth, c_sourceHeight)
    , m_x(x)
    , m_speed(1)
    , m_direction(DIRECTION_STRAIGHT)
{
    m_sprite.setSize(32, config::c_screenHeight);
    m_sprite.setColourMod(0x00, 0xff, 0x00);
}

void RacingLine::update( units::MS deltaTime )
{
    if (m_direction == DIRECTION_LEFT) {
	m_x -= c_speed * m_speed * deltaTime;
    } else if (m_direction == DIRECTION_RIGHT) {
	m_x += c_speed * m_speed * deltaTime;
    }
}

void RacingLine::draw( Graphics& graphics )
{
    m_sprite.draw(graphics, m_x - c_sourceWidth * 0.5f, 0.0f);
}

void RacingLine::startMovingLeft()
{
    m_direction = DIRECTION_LEFT;
}

void RacingLine::startMovingRight()
{
    m_direction = DIRECTION_RIGHT;
}

void RacingLine::stopMoving()
{
    m_direction = DIRECTION_STRAIGHT;
}

void RacingLine::setSpeed(int speed)
{
    m_speed = std::min(9, std::max(0, speed));
}

units::Game RacingLine::getX() const
{
    return m_x;
}

units::Game RacingLine::getRateOfChange() const
{
    if (m_direction == DIRECTION_LEFT) {
	return -c_speed * m_speed;
    } else if (m_direction == DIRECTION_RIGHT) {
	return c_speed * m_speed;
    } else {
	return 0.0f;
    }
}

void RacingLine::setColour( Uint8 red, Uint8 green, Uint8 blue )
{
    m_sprite.setColourMod(red, green, blue);
}
