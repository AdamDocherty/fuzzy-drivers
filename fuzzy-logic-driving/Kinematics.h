#ifndef Kinematics_h__
#define Kinematics_h__

#include "Units.h"

class Kinematics {
public:
    Kinematics(units::Game position, units::Velocity velocity)
        : position(position), velocity(velocity) {}

    units::Game position;
    units::Velocity velocity;

    units::Game delta(units::MS deltaTime) const { return velocity * deltaTime; }
};

#endif // Kinematics_h__
