#include "Sprite.h"

#include "Graphics.h"

Sprite::Sprite( Graphics& graphics, const std::string& filename, units::Pixel sourceX, 
    units::Pixel sourceY, units::Pixel width, units::Pixel height )
{
    const bool blackIsTransparent = true;
    m_spriteSheet = graphics.loadImage(filename, blackIsTransparent);
    m_sourceRect.x = sourceX;
    m_sourceRect.y = sourceY;
    m_sourceRect.w = width;
    m_sourceRect.h = height;
    m_size.x = width;
    m_size.y = height;
}

void Sprite::draw( Graphics& graphics, units::Game x, units::Game y, units::Degrees angle /* = 0.0f */, SDL_RendererFlip flip /* = SDL_FLIP_NONE */ )
{
    SDL_Rect destRect;
    destRect.x = units::gameToPixel(x);
    destRect.y = units::gameToPixel(y);
    destRect.w = m_size.x;
    destRect.h = m_size.y;

    graphics.renderTexture(m_spriteSheet, &m_sourceRect, &destRect, angle, NULL, flip);
}

void Sprite::setSize( units::Pixel x, units::Pixel y )
{
    m_size.x = x;
    m_size.y = y;
}

void Sprite::setColourMod( Uint8 red, Uint8 green, Uint8 blue )
{
    SDL_SetTextureColorMod(m_spriteSheet, red, green, blue);
}
