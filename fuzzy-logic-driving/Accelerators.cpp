#include "Accelerators.h"

#include <algorithm>

#include "Kinematics.h"

const ZeroAccelerator ZeroAccelerator::c_zero;

void FrictionAccelerator::updateVelocity(Kinematics& kinematics, units::MS deltaTime) const {
    kinematics.velocity = kinematics.velocity > 0.0f ?
	std::max(0.0f, kinematics.velocity - m_friction * deltaTime) :
        std::min(0.0f, kinematics.velocity + m_friction * deltaTime);
}

void ConstantAccelerator::updateVelocity(Kinematics& kinematics, units::MS deltaTime) const {
    if (m_acceleration < 0.0f) {
	kinematics.velocity = std::max(kinematics.velocity + m_acceleration * deltaTime, m_maxVelocity);
    } else {
	kinematics.velocity = std::min(kinematics.velocity + m_acceleration * deltaTime, m_maxVelocity);
    }
}