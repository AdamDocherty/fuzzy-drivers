#ifndef RacingLine_h__
#define RacingLine_h__

#include "Sprite.h"
#include "Units.h"

class Graphics;

class RacingLine {
public:
    RacingLine(Graphics& graphics, units::Game x);

    void update(units::MS deltaTime);
    void draw(Graphics& graphics);

    void startMovingLeft();
    void startMovingRight();
    void stopMoving();

    void setSpeed(int speed);
    void setColour(Uint8 red, Uint8 green, Uint8 blue);

    units::Game getX() const;
    units::Game getRateOfChange() const;
private:
    enum DirectionType {
	DIRECTION_STRAIGHT = 0,
	DIRECTION_RIGHT, 
	DIRECTION_LEFT
    };

    Sprite m_sprite;
    units::Game m_x;
    int m_speed;

    DirectionType m_direction;
};



#endif // RacingLine_h__
