#include "Timer.h"

std::set<Timer*> Timer::s_timers;

Timer::Timer( units::MS expirationTime, bool startActive /*= false*/ )
    : m_curentTime(startActive ? 0 : expirationTime)
    , m_expirationTime(expirationTime)
{
    s_timers.insert(this);
}

Timer::~Timer()
{
    s_timers.erase(this);
}

void Timer::reset()
{
    m_curentTime = 0;
}

bool Timer::active() const
{
    return currentTime() < m_expirationTime;
}

bool Timer::expired() const
{
    return !active();
}

units::MS Timer::currentTime() const
{
    return m_curentTime;
}

void Timer::updateAll( units::MS deltaTime )
{
    for (auto it = s_timers.begin(); it != s_timers.end(); ++it) {
	(*it)->update(deltaTime);
    }
}

void Timer::update( units::MS deltaTime )
{
    if (active()) {
	m_curentTime += deltaTime;
    }
}
