#include "Graphics.h"

#include <SDL.h>

#include "Game.h"
#include "Config.h"

namespace {

const int c_bitsPerPixel = 32;

// RBG values for the colour key
const Uint8 c_colourKey[3] = { 255, 174, 201 };

}

Graphics::Graphics()
    : m_window(NULL)
    , m_screen(NULL)
    , m_renderer(NULL)
{
    m_window = SDL_CreateWindow("Fuzzy Driving", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, config::c_screenWidth, config::c_screenHeight, 0);
    m_screen = SDL_GetWindowSurface(m_window);
    m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor(m_renderer, 0x00, 0x00, 0x20, 0xFF);
#ifndef _DEBUG
    SDL_ShowCursor(SDL_DISABLE);
#endif// _DEBUG
}

Graphics::~Graphics()
{
    for (auto it = m_spriteSheets.begin(); it != m_spriteSheets.end(); ++it) {
        SDL_DestroyTexture(it->second);
        //SDL_FreeSurface(it->second);
    }
    SDL_FreeSurface(m_screen);
    SDL_DestroyWindow(m_window);
}

Graphics::TextureID Graphics::loadImage( const std::string& filename, bool colourKey /*= false*/ )
{
    const std::string filepath = "content/" + filename + ".bmp";
    // if we have not loaded in the spritesheet
    if (m_spriteSheets.count(filepath) == 0) {
	// load it in now
	SDL_Surface* loadedSurface = SDL_LoadBMP(filepath.c_str());
        if (colourKey) {
	    const Uint32 black_colour = SDL_MapRGB(loadedSurface->format, c_colourKey[0], c_colourKey[1], c_colourKey[2]);
	    SDL_SetColorKey(loadedSurface, SDL_TRUE, black_colour);
        }
	m_spriteSheets[filepath] = SDL_CreateTextureFromSurface(m_renderer, loadedSurface);
	SDL_FreeSurface(loadedSurface);
    }
    return m_spriteSheets[filepath];
}

void Graphics::renderTexture( TextureID source, SDL_Rect* sourceRect, SDL_Rect* destRect, units::Degrees angle, SDL_Point* center, SDL_RendererFlip flip )
{
    //SDL_RenderCopy(m_renderer, source, sourceRect, destRect);
    SDL_RenderCopyEx(m_renderer, source, sourceRect, destRect, angle, center, flip);
}

void Graphics::clear()
{
    SDL_RenderClear(m_renderer);
}

void Graphics::flip()
{
    SDL_RenderPresent(m_renderer);
}
