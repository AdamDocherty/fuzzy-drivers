#ifndef Input_h__
#define Input_h__

#include <SDL.h>
#include <map>

class Input {
public:
    void beginNewFrame();

    void keyDownEvent(const SDL_Event& event);
    void keyUpEvent(const SDL_Event& event);

    bool wasKeyPressed(SDL_Keycode key);
    bool wasKeyReleased(SDL_Keycode key);
    bool isKeyHeld(SDL_Keycode key);

private:
    typedef std::map<SDL_Keycode, bool> KeyMap;

    KeyMap m_heldKeys;
    KeyMap m_pressedKeys;
    KeyMap m_releasedKeys;
};

#endif // Input_h__
