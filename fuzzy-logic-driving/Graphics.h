#ifndef Graphics_h__
#define Graphics_h__

#include <map>
#include <string>
#include <SDL.h>

#include "Units.h"

class Graphics {
public:
    typedef SDL_Texture* TextureID;

    Graphics();
    ~Graphics();

    TextureID loadImage(const std::string& filename, bool blackIsTransparent = false);
    void renderTexture(TextureID source, SDL_Rect* sourceRect, SDL_Rect* destRect, units::Degrees angle, SDL_Point* center, SDL_RendererFlip flip);
    void clear();
    void flip();

private:
    typedef std::map<std::string, TextureID> SpriteMap;

    SpriteMap m_spriteSheets;
    SDL_Window* m_window;
    SDL_Surface* m_screen;
    SDL_Renderer* m_renderer;

};

#endif // Graphics_h__
