#include <SDL.h>

#include <typeinfo>
#include <iomanip>
#include <cstdlib>
#include <fstream>

#include "fl_examples.h"
#include "FuzzySteeringController.h"
#include "Game.h"

void testFuzzyController(const FuzzySteeringController& fuzzyController);
void printOutput(const FuzzySteeringController& fuzzyController, fl::scalar distance, fl::scalar rateOfChange);
void printAllTestCases(const FuzzySteeringController& fuzzyController);

int main(int argc, char** argv) {
    FuzzySteeringController fuzzyController;

    if (argc <= 1) {    
        Game game(fuzzyController);
    }

    testFuzzyController(fuzzyController);

    try {
        fuzzylite::example::examples();
    } catch (fl::Exception& e) {
        FL_LOG(e.what());
        FL_LOG(e.btCallStack());
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void testFuzzyController( const FuzzySteeringController& fuzzyController )
{
    using std::cout;
    using std::endl;
    using std::cin;

    cout << "To test the Fuzzy Steering Controller, please input values in the following format:" << endl <<
        "<distance> <rateOfChange>" << endl <<
        "where both values are real numbers in the range (-1.8, 1.8)" << endl << endl;

    std::string input = "";

    while(true) {
        fl::scalar distance;
        fl::scalar rateOfChange;

        std::getline(cin, input);
        std::stringstream stream(input);
        if (stream >> distance >> rateOfChange) {
            printOutput(fuzzyController, distance, rateOfChange);
        } else if (input == "all") {
            printAllTestCases(fuzzyController);
        } else {
            cout << "Invalid input, Please input two numerical values for distance and rate of change. Or 'all' to print all test cases" << endl;
        }
    }
}

void printOutput(const FuzzySteeringController& fuzzyController, fl::scalar distance, fl::scalar rateOfChange) 
{
    using namespace std;

    fl::scalar acceleration = fuzzyController.calculateAcceleration(distance, rateOfChange);

    cout << "Distance.input = " << distance << ", RateOfChange.input = " << rateOfChange << endl <<
        "Acceleration.output = " << fl::Op::str(acceleration) << endl << endl;
}

void printAllTestCases(const FuzzySteeringController& fuzzyController) 
{
    fuzzyController.printAllResults();
}
