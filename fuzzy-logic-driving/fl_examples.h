#ifndef fl_examples_h__
#define fl_examples_h__

#include <fl/Headers.h>

namespace fuzzylite {
    void initFuzzylite();
    namespace example {
	void examples() throw (fl::Exception);
	}
}

#endif // fl_examples_h__
