#include "FuzzySteeringController.h"

#include <signal.h>
#include <iostream>
#include <algorithm>

#include "Config.h"

using namespace fl;

namespace {

    void initFuzzylite() {
        std::set_terminate(fl::Exception::terminate);
        std::set_unexpected(fl::Exception::terminate);
        signal(SIGSEGV, fl::Exception::signalHandler);
        signal(SIGABRT, fl::Exception::signalHandler);
        signal(SIGILL, fl::Exception::signalHandler);
        signal(SIGSEGV, fl::Exception::signalHandler);
        signal(SIGFPE, fl::Exception::signalHandler);
#ifdef FL_UNIX
        signal(SIGBUS, fl::Exception::signalHandler);
        signal(SIGPIPE, fl::Exception::signalHandler);
#endif
    }



} // end anonymous namespace

FuzzySteeringController::FuzzySteeringController()
{
    initFuzzylite();
    m_engine.reset(new Engine("car-controller"));

    m_distanceInput = new InputVariable("Distance", -1.8, 1.8);
    // Add Terms for distance variables
    m_distanceInput->addTerm(new Triangle("FAR_LEFT", -1.8, -1.0, -0.2));
    m_distanceInput->addTerm(new Triangle("LEFT", -1.3, -0.5, 0.3));
    m_distanceInput->addTerm(new Triangle("CENTRE", -0.8, 0.0, 0.8));
    m_distanceInput->addTerm(new Triangle("RIGHT", -0.3, 0.5, 1.3));
    m_distanceInput->addTerm(new Triangle("FAR_RIGHT", 0.2, 1.0, 1.8));
    // ownership of m_distanceInput is passed to m_engine
    m_engine->addInputVariable(m_distanceInput);

    m_rateOfChangeInput = new InputVariable("RateOfChange", -1.8, 1.8);
    // Add Terms for rate of change variable
    m_rateOfChangeInput->addTerm(new Triangle("FAST_LEFT", -1.8, -1.0, -0.2));
    m_rateOfChangeInput->addTerm(new Triangle("LEFT", -1.3, -0.5, 0.3));
    m_rateOfChangeInput->addTerm(new Triangle("ZERO", -0.8, 0.0, 0.8));
    m_rateOfChangeInput->addTerm(new Triangle("RIGHT", -0.3, 0.5, 1.3));
    m_rateOfChangeInput->addTerm(new Triangle("FAST_RIGHT", 0.2, 1.0, 1.8));
    // ownership of m_rateOfChangeInput is passed to m_engine
    m_engine->addInputVariable(m_rateOfChangeInput);

    m_accelerationOutput = new OutputVariable("Acceleration", -1.8, 1.8);
    // Add Terms for acceleration output
    m_accelerationOutput->addTerm(new Triangle("HARD_LEFT", -1.8, -1.0, -0.2));
    m_accelerationOutput->addTerm(new Triangle("LEFT", -1.3, -0.5, 0.3));
    m_accelerationOutput->addTerm(new Triangle("NONE", -0.8, 0.0, 0.8));
    m_accelerationOutput->addTerm(new Triangle("RIGHT", -0.3, 0.5, 1.3));
    m_accelerationOutput->addTerm(new Triangle("HARD_RIGHT", 0.2, 1.0, 1.8));
    // ownership of m_accelerationOutput is passed to m_engine
    m_engine->addOutputVariable(m_accelerationOutput);

    RuleBlock* ruleBlock = new RuleBlock();
    // Add rules to the ruleblock
    ruleBlock->addRule(FuzzyRule::parse("if Distance is FAR_LEFT and RateOfChange is FAST_LEFT then Acceleration is HARD_RIGHT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is FAR_LEFT and RateOfChange is LEFT then Acceleration is HARD_RIGHT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is FAR_LEFT and RateOfChange is ZERO then Acceleration is RIGHT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is FAR_LEFT and RateOfChange is RIGHT then Acceleration is RIGHT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is FAR_LEFT and RateOfChange is FAST_RIGHT then Acceleration is NONE", m_engine.get()));

    ruleBlock->addRule(FuzzyRule::parse("if Distance is LEFT and RateOfChange is FAST_LEFT then Acceleration is HARD_RIGHT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is LEFT and RateOfChange is LEFT then Acceleration is RIGHT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is LEFT and RateOfChange is ZERO then Acceleration is RIGHT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is LEFT and RateOfChange is RIGHT then Acceleration is NONE", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is LEFT and RateOfChange is FAST_RIGHT then Acceleration is LEFT", m_engine.get()));

    ruleBlock->addRule(FuzzyRule::parse("if Distance is CENTRE and RateOfChange is FAST_LEFT then Acceleration is RIGHT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is CENTRE and RateOfChange is LEFT then Acceleration is RIGHT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is CENTRE and RateOfChange is ZERO then Acceleration is NONE", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is CENTRE and RateOfChange is RIGHT then Acceleration is LEFT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is CENTRE and RateOfChange is FAST_RIGHT then Acceleration is LEFT", m_engine.get()));

    ruleBlock->addRule(FuzzyRule::parse("if Distance is RIGHT and RateOfChange is FAST_LEFT then Acceleration is RIGHT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is RIGHT and RateOfChange is LEFT then Acceleration is NONE", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is RIGHT and RateOfChange is ZERO then Acceleration is LEFT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is RIGHT and RateOfChange is RIGHT then Acceleration is LEFT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is RIGHT and RateOfChange is FAST_RIGHT then Acceleration is HARD_LEFT", m_engine.get()));

    ruleBlock->addRule(FuzzyRule::parse("if Distance is FAR_RIGHT and RateOfChange is FAST_LEFT then Acceleration is NONE", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is FAR_RIGHT and RateOfChange is LEFT then Acceleration is LEFT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is FAR_RIGHT and RateOfChange is ZERO then Acceleration is LEFT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is FAR_RIGHT and RateOfChange is RIGHT then Acceleration is HARD_LEFT", m_engine.get()));
    ruleBlock->addRule(FuzzyRule::parse("if Distance is FAR_RIGHT and RateOfChange is FAST_RIGHT then Acceleration is HARD_LEFT", m_engine.get()));

    // ownership of the RuleBlock is passsed to m_engine
    m_engine->addRuleBlock(ruleBlock);

    m_engine->configure("Minimum", "Maximum", "Minimum", "Maximum", "Centroid", FL_DIVISIONS);

    return;
}

FuzzySteeringController::~FuzzySteeringController()
{

}

double FuzzySteeringController::calculateAcceleration( fl::scalar distance, fl::scalar rateOfChange ) const
{
    //fl::scalar normalisedDistance = normaliseDistance(distance);

    // Must clamp rate of change to the min and max values of the fuzzy logic sets
    // A value outside the range (-1.8, 1.8) would return NaN in fuzzylite
    //rateOfChange = std::max(-1.799, std::min(1.799, rateOfChange));

    m_distanceInput->setInput(distance);
    m_rateOfChangeInput->setInput(rateOfChange);

    m_engine->process();

    fl::scalar acceleration = m_accelerationOutput->defuzzify();

    return acceleration;
}

void FuzzySteeringController::printAllResults() const
{
    fl::scalar distanceRange = m_distanceInput->getMaximum() - m_distanceInput->getMinimum();
    fl::scalar rocRange = m_rateOfChangeInput->getMaximum() - m_rateOfChangeInput->getMinimum();
    int steps = 20;
    for (int i = 1; i < steps; ++i) {
        fl::scalar distance = m_distanceInput->getMinimum() + i * (distanceRange / steps);
        m_distanceInput->setInput(distance);
        for (int j = 1; j < steps; ++j) {
            fl::scalar roc = m_rateOfChangeInput->getMinimum() + j *(rocRange / steps);
            m_rateOfChangeInput->setInput(roc);
            m_engine->process();
            std::cout << "Distance.input = " << fl::Op::str(distance) << "\t" <<
                "RateOfChange.input = " << fl::Op::str(roc) << " -> " << std::endl <<
                "Acceleration.output = " << fl::Op::str(m_accelerationOutput->defuzzify()) << std::endl;
        }
    }
}
