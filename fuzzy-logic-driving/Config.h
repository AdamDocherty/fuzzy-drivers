#ifndef Config_h__
#define Config_h__

namespace config {

enum GraphicsQuality {
    HIGH_QUALITY,
    ORIGINAL_QUALITY
};

inline GraphicsQuality getGraphicsQuality() { return HIGH_QUALITY; }

const int c_screenWidth = 1080;
const int c_screenHeight = 720;

}

#endif // Config_h__
